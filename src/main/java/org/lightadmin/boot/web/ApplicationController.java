package org.lightadmin.boot.web;

import org.lightadmin.boot.repository.TerminalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApplicationController {

    @Autowired
    private TerminalRepository terminalRepository;

    @RequestMapping("/")
    public String thymeleafIndexPage(Model model) {
        model.addAttribute("Terminals", terminalRepository.findAll());
        return "index";
    }
}