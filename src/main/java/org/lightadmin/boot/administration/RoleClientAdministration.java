package org.lightadmin.boot.administration;

import org.lightadmin.api.config.AdministrationConfiguration;
import org.lightadmin.api.config.builder.EntityMetadataConfigurationUnitBuilder;
import org.lightadmin.api.config.unit.EntityMetadataConfigurationUnit;
import org.lightadmin.boot.domain.RoleClient;
import org.lightadmin.boot.domain.User;

import static org.lightadmin.api.config.utils.EnumElement.element;

public class RoleClientAdministration extends AdministrationConfiguration<RoleClient> {

    @Override
    public EntityMetadataConfigurationUnit configuration(EntityMetadataConfigurationUnitBuilder configurationBuilder) {
        return configurationBuilder
                .nameField("roleclient").singularName("RoleClient").pluralName("RoleClients")
                .field( "role" ).enumeration(
                        element( "ADMIN", "Admin" ),
                        element( "INSPECTOR", "Inspector" ),
                        element( "CONSUMER", "Consumer" ))
                .build();
    }
}