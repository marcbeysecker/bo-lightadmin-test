package org.lightadmin.boot.repository;

import org.lightadmin.boot.domain.Terminal;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TerminalRepository extends PagingAndSortingRepository<Terminal, Long> {

}