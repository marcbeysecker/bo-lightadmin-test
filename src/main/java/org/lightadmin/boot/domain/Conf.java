package org.lightadmin.boot.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Conf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany
    @JoinTable(name = "client_conf",
            joinColumns = @JoinColumn(name = "client_id", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "conf_id", referencedColumnName = "ID")
    )
    private Set<Client> clients;

    @Column
    private String name;

    @Column
    private String zip;

    public Long getId() {
        return id;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public String getName() {
        return name;
    }

    public String getZip() {
        return zip;
    }

    @Override
    public String toString() {
        return "Conf{" +
                "id=" + id +
                ", clients=" + clients +
                ", name='" + name + '\'' +
                ", zip='" + zip + '\'' +
                '}';
    }
}
