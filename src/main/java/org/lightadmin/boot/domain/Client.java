package org.lightadmin.boot.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String serverUrl;

    @Column
    private String name;

    public Long getId() {
        return id;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", serverUrl='" + serverUrl + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
