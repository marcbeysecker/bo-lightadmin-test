package org.lightadmin.boot.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Terminal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @ManyToOne
    private User user;

    @NotBlank
    @Column
    private String name;

    @NotBlank
    @Column
    private String imei;

    public Terminal() {
    }

    public Terminal(User user, String name, String imei) {
        this.user = user;
        this.name = name;
        this.imei = imei;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return this.user;
    }

    public String getName() {
        return this.name;
    }

    public String getImei() {
        return this.imei;
    }
}