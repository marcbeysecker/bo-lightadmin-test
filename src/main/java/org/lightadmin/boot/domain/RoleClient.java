package org.lightadmin.boot.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class RoleClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToOne
    private User user;

    @OneToOne
    private Client client;

    public Long getId() {
        return id;
    }

    public Role getRole() {
        return role;
    }

    public User getUser() {
        return user;
    }

    public Client getClient() {
        return client;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "RoleClient{" +
                "id=" + id +
                ", role=" + role +
                ", user=" + user +
                ", client=" + client +
                '}';
    }
}
