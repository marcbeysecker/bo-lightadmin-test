package org.lightadmin.boot.domain;


public enum Role {
    ADMIN, INSPECTOR, CONSUMER
}
